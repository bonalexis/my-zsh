### Chargements des outils
#  commandes antigen
source $HOME/.antigen.zsh

# commande pour avoir la branche git
autoload -Uz vcs_info
zstyle ':vcs_info:git:*' formats ' %b'
precmd() { vcs_info }





### Antigen
antigen use oh-my-zsh

antigen bundle git
antigen bundle pip
antigen bundle command-not-found

antigen bundle zsh-users/zsh-completions
antigen bundle zsh-users/zsh-autosuggestions
antigen bundle zsh-users/zsh-syntax-highlighting
antigen bundle zsh-users/zsh-history-substring-search
antigen bundle lukechilds/zsh-nvm

antigen apply





### Prompt
PROMPT='%F{cyan}%T%f %F{magenta}%n%f@%F{magenta}%m%f:%F{blue}%~%f%F{yellow}${vcs_info_msg_0_}%f $ '
RPROMPT='[%?]'

# Path
PATH=.:$HOME/bin:$PATH

export PATH="$PATH:/home/alexis/.foundry/bin"


# Load Angular CLI autocompletion.
source <(ng completion script)

# Created by `pipx` on 2023-08-04 19:57:50
export PATH="$PATH:/home/alexis/.local/bin"
